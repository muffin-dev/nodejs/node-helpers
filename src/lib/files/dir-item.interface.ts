/**
 * @interface IDirItem Contains data of a folder or a file such as path and name.
 */
export interface IDirItem {
  /**
   * @var isFile Is this item a file?
   */
  isFile: boolean;

  /**
   * @var isDirectory Is this item a directory?
   */
  isDirectory: boolean;

  /**
   * @var name Name of the file (without extension) or the directory.
   */
  name: string;

  /**
   * @var fullName Name of the file (including extension) or the directory.
   */
  fullName: string;

  /**
   * @var extension Extension of the file. If the item is a directory, this is null.
   */
  extension?: string | null;

  /**
   * @var path Relative path to the file or the directory.
   */
  path: string;
}
