import * as FileSystem from 'fs';
import * as Path from 'path';

import { IDirItem } from './dir-item.interface';

/**
 * @type Represents the options that can be passed for reading a file.
 */
export type TReadFileOptions = string | { encoding: BufferEncoding, flag?: string };

//#region Public API

/**
 * Reads a file asynchronously.
 * Note that it uses NodeJS's FileSystem.readFile() method, but transforming the callback into a promise.
 * @async
 * @param path Path to the file.
 * @param options If set, defines the data encoding. If null, uses "utf8" by default. For more informations, see the documentation of
 * FileSystem.readFile() method on the Node JS official documentation:
 * https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_readfile_path_options_callback
 * @param ignoreMissingFile If true, returns null instead of throwing an error if the file at the given path doesn't exist.
 * @returns Returns the file content if the file have been read successfully.
 * @throws {NodeJS.ErrnoException} Thrown if the file doesn't exist.
 */
export function readFileAsync(
  path: string,
  options: TReadFileOptions = null,
  ignoreMissingFile = false
): Promise<string> {
  return new Promise((resolve: (data: string) => void, reject: (error: NodeJS.ErrnoException) => void) => {
    if (options === null) {
      options = 'utf8';
    }

    FileSystem.readFile(path, options, (error: NodeJS.ErrnoException, data: string) => {
      if (error) {
        if (ignoreMissingFile) {
          resolve(null);
        }
        else {
          reject(error);
        }

        return;
      }
      resolve(data);
    });
  });
}

/**
 * Read the content of a directory.
 * @async
 * @param path Path to the directory to read.
 * @param includeDirectories False by default. Defines if directories must be queried.
 * @param includeFiles True by default. Defines if files must be queried.
 * @param extensions Null by default. If includeFiles parameter is set to true, and this value is not null (could be a string or an array
 * of strings), filters only files with the specified extension (or one of them if an array is passed). Note that extensions passed must
 * not contain ".".
 * @param ignoreMissingFile False by default. If true, returns an empty array instead of throwing an error if the folder at given path
 * doesn't exist.
 * @param recursive False by default. If true, gets recursively files and folders into folders.
 * @param ignoreExtensions False by default. Reverse the "extensions" parameter behavior. If true and the extensions parameter is not
 * null, include all found files but the one with the named extensions.
 * @returns Returns an array of all the found files and directories.
 * @throws {NodeJS.ErrnoException} Thrown if the folder doesn't exist, and ignoreMissingFolder parameter is set to false.
 */
export function readdirAsync(
  path: string,
  includeDirectories = false,
  includeFiles = true,
  extensions: string | string[] | null = null,
  ignoreMissingFolder = false,
  recursive = false,
  ignoreExtensions = false
): Promise<IDirItem[]> {
  return _readdirAsync(path, includeDirectories, includeFiles, extensions, ignoreMissingFolder, recursive, ignoreExtensions);
}

/**
 * Reads a JSON file asynchronously.
 * @async
 * @param path Path to the JSON file.
 * @param options If set, defines the data encoding. If null, uses "utf8" by default. For more informations, see the documentation of
 * FileSystem.readFile() method on the Node JS official documentation:
 * https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_readfile_path_options_callback
 * @param ignoreMissingFile False by default. If true, returns an empty object instead of throwing an error if the file doesn't exist.
 * @returns Returns the parsed JSON data.
 * @throws {NodeJS.ErrnoException} Thrown if the file doesn't exist, and ignoreMissingFile paramater is set to false.
 * @throws {SyntaxError} Thrown if the JSON file content is not valid.
 */
export async function readJSONFileAsync(
  path: string,
  options: TReadFileOptions = null,
  ignoreMissingFile = false
): Promise<unknown | null> {
  try {
    const data = await readFileAsync(path, options);
    return JSON.parse(data);
  }
  catch (error) {
    if (ignoreMissingFile && error.code && error.code === 'ENOENT') {
      return { };
    }
    else {
      throw error;
    }
  }
}

/**
 * Writes a file asynchronously. Do the same as fs.writeFile(), but creates folders to the given path if they don't exist.
 * @async
 * @param path Path to the file to write.
 * @param data Data to write into the file.
 * @param options Null by default. Options for the file to write. If null given, uses "utf8" by default.
 * @returns {Promise<void>}
 * @throws {Error} Throws the error that occured when making a directory or writing the file.
 */
export function writeFileAsync(
  path: string,
  data: string | NodeJS.ArrayBufferView,
  options: FileSystem.WriteFileOptions = null
): Promise<void> {
  if (options === null) {
    options = 'utf8';
  }

  return new Promise((resolve: () => void, reject: (error: NodeJS.ErrnoException) => void) => {
    FileSystem.mkdir(Path.dirname(path), { recursive: true }, (makeDirError: NodeJS.ErrnoException) => {
      if (makeDirError) {
        reject(makeDirError);
        return;
      }

      FileSystem.writeFile(path, data, options, (writeFileError: NodeJS.ErrnoException) => {
        if (writeFileError) {
          reject(writeFileError);
        }
        else {
          resolve();
        }
      });
    });
  });
}

/**
 * Copies a file asynchronously. Do the same as fs.copyFile(), but creates the missing directories to the given target if they don't exist.
 * @async
 * @param source Path to the source file to copy.
 * @param target Path to the copy destination.
 */
export function copyFileAsync(source: string, target: string): Promise<void> {
  return new Promise((resolve: () => void, reject: (error: NodeJS.ErrnoException) => void) => {
    FileSystem.mkdir(Path.dirname(target), { recursive: true }, (makeDirError: NodeJS.ErrnoException) => {
      if (makeDirError) {
        reject(makeDirError);
        return;
      }

      FileSystem.copyFile(source, target, (copyError: NodeJS.ErrnoException) => {
        if (copyError) {
          reject(copyError);
        }
        else {
          resolve();
        }
      });
    });
  });
}

/**
 * Copies an entire directory from given source path to given target path.
 * Note that target folders are created if they don't exist.
 * @async
 * @param source Path to the original directory to copy
 * @param target Path to the copied files destination.
 */
export function copyDirectory(source: string, target: string): Promise<void> {
  // If source or target is relative, makes the path absolute from process.cwd().
  if (!Path.isAbsolute(source)) {
    source = Path.join(process.cwd(), source);
  }
  if (!Path.isAbsolute(target)) {
    target = Path.join(process.cwd(), target);
  }

  return new Promise((resolve: () => void, reject: (error: NodeJS.ErrnoException) => void) => {
    readdirAsync(source, false, true, null, false, true)
      .then(items => {
        const copyPromises = new Array<Promise<void>>();
        // For each item in the directory to copy
        items.forEach(item => {
          // Copy the file
          const destination = Path.join(target, item.path.slice(source.length));
          copyPromises.push(copyFileAsync(item.path, destination));
        });

        // Resolve promise only when all files are copied.
        Promise.all(copyPromises)
          .then(() => {
            resolve();
          })
          .catch(error => {
            reject(error);
          });
      })
      .catch(error => {
        reject(error);
      });
  });
}

/**
 * Removes a file at the given path asynchronously.
 * @async
 * @param path Path to the file to remove.
 * @param ignoreMissingFile False by default. If true, this method won't throw error if the file doesn't exist.
 */
export function removeFile(path: string, ignoreMissingFile = false): Promise<void> {
  return new Promise((resolve: () => void, reject: (error: unknown) => void) => {
    FileSystem.stat(path, (error: NodeJS.ErrnoException, stat: FileSystem.Stats) => {
      if (error) {
        if (ignoreMissingFile && error.code && error.code === 'ENOENT') {
          resolve();
          return;
        }

        reject(error);
        return;
      }

      if (stat.isDirectory()) {
        removeDirectory(path, ignoreMissingFile)
          .then(() => { resolve(); })
          .catch((removeDirectoryError: unknown) => { reject(removeDirectoryError); });
      }
      else {
        FileSystem.unlink(path, (unlinkError: unknown) => {
          if (unlinkError) {
            reject(unlinkError);
          }
          else {
            resolve();
          }
        });
      }
    });
  });
}

/**
 * Removes the directory at the given path, even if it's not empty.
 * @async
 * @param path Path to the directory to remove.
 * @param ignoreMissingFile False by default. If true, don't throw error if the folder doesn't exist.
 */
export async function removeDirectory(path: string, ignoreMissingFolder = false): Promise<void> {
  let files: IDirItem[] = [];
  try {
    files = await readdirAsync(path, true, true, null, false);
  }
  catch (error) {
    if (ignoreMissingFolder && error.code && error.code === 'ENOENT') {
      return;
    }
    throw error;
  }

  const promises = new Array<Promise<void>>();

  const count = files.length;
  for (let i = 0; i < count; i++) {
    if (files[i].isDirectory) {
      promises.push(removeDirectory(files[i].path));
    }
    else {
      promises.push(_removeFileAsyncVerified(files[i]));
    }
  }

  // Wait for all files and subfolders to be removed
  await Promise.all(promises)
    .then(async () => {
      // After removing all files and subfolders, finally remove the folder at the given path
      await _removeFolderAsyncVerified(path);
    })
    .catch(error => { throw error; });
}

//#endregion

//#region Private API

/**
 * Read the content of a directory.
 * @async
 * @param path Path to the directory to read.
 * @param includeDirectories False by default. Defines if directories must be queried.
 * @param includeFiles True by default. Defines if files must be queried.
 * @param extensions Null by default. If includeFiles parameter is set to true, and this value is not null (could be a
 * string or an array of strings), filters only files with the specified extension (or one of them if an array is passed). Note that
 * extensions passed must not contain ".".
 * @param ignoreMissingFile False by default. If true, returns an empty array instead of throwing an error if the folder at given path
 * doesn't exist.
 * @param recursive False by default. If true, gets recursively files and folders into folders.
 * @param ignoreExtensions False by default. Reverse the "extensions" parameter behavior. If true and the extensions parameter is not
 * null, include all found files but the one with the named extensions.
 * @param outputItems Null by default This parameter is used for recusrive calls to this method. Contains an array of found
 * items.
 * @returns returns an array of found files and directories.
 * @throws {NodeJS.ErrnoException} Thrown if the folder doesn't exist, and ignoreMissingFolder paramater is set to false.
 */
function _readdirAsync(
  path: string,
  includeDirectories = false,
  includeFiles = true,
  extensions: string | string[] | null = null,
  ignoreMissingFolder = false,
  recursive = false,
  ignoreExtensions = false,
  outputItems: IDirItem[] = []
): Promise<IDirItem[]> {
  // If the given extension is only one string
  if (typeof extensions === 'string') {
    // Compute it as an array with one entry
    extensions = [extensions];
  }
  // If the extenstions parameter is not an array, ignore it
  if (!Array.isArray(extensions)) {
    extensions = null;
  }

  return new Promise((resolve: (data: IDirItem[]) => void, reject: (error: unknown) => void) => {
    // If neither directories nor files are included, resolves the promises by returning an empty array.
    if (!includeDirectories && !includeFiles) {
      resolve([]);
    }

    FileSystem.readdir(path, { withFileTypes: true }, (error: NodeJS.ErrnoException, files: FileSystem.Dirent[]) => {
      if (error) {
        // If missing folder error is ignored, resolves the promise by returning an empty array.
        if (ignoreMissingFolder && error.code && error.code === 'ENOENT') {
          resolve([]);
        }
        // Otherwise, reject the promise by sending the error.
        reject(error);
        return;
      }

      if (outputItems === null || !Array.isArray(outputItems)) {
        outputItems = new Array<IDirItem>();
      }

      const promises = new Array<Promise<IDirItem[]>>();
      const filesLength = files.length;
      for (let i = 0; i < filesLength; i++) {
        const file = files[i];

        const outputEntry: IDirItem = {
          fullName: file.name,
          isDirectory: false,
          isFile: false,
          name: file.name,
          path: Path.join(path, file.name)
        };

        // If the current item is a directory
        if (file.isDirectory()) {
          // If directories are included to the query, add this entry
          if (includeDirectories) {
            outputEntry.isDirectory = true;
            outputItems.push(outputEntry);
          }

          // If user required recursive operation
          if (recursive) {
            // Call readdirAsync() recursively for the current directory item
            promises.push(_readdirAsync(
              outputEntry.path,
              includeDirectories,
              includeFiles,
              extensions,
              ignoreMissingFolder,
              recursive,
              ignoreExtensions,
              outputItems
            ));
          }
        }

        // Else, if it's a file but they're queried
        else if (!file.isDirectory() && includeFiles) {
          outputEntry.isFile = true;
          // Process extension and name without extension
          const extensionIndex = outputEntry.fullName.lastIndexOf('.');
          outputEntry.extension = outputEntry.fullName.slice(extensionIndex + 1);
          outputEntry.name = outputEntry.fullName.slice(0, extensionIndex);

          // If extensions are specified
          if (extensions !== null) {
            const extensionsLength = extensions.length;
            // For each extension
            for (let j = 0; j < extensionsLength; j++) {
              if (ignoreExtensions) {
                // If the file name doesn't contain the extension, add it to the output files
                if (!(file.name.slice(-extensions[j].length - 1) === '.' + extensions[j])) {
                  outputItems.push(outputEntry);
                  break;
                }
              }
              else {
                // If the file name contains that extension, add it to the output files
                if (file.name.slice(-extensions[j].length - 1) === '.' + extensions[j]) {
                  outputItems.push(outputEntry);
                  break;
                }
              }
            }
          }
          // Else, if no extension is specified
          else {
            outputItems.push(outputEntry);
          }
        }
      }

      Promise.all(promises)
        .then(() => {
          resolve(outputItems);
        })
        .catch(recursiveError => {
          reject(recursiveError);
        });
    });
  });
}

/**
 * Removes a folder item asynchronously.
 * Used by removeDirectory().
 * @async
 * @param item The item to remove. Assumes that the given item has already been checked as an existing file.
 */
function _removeFileAsyncVerified(item: IDirItem): Promise<void> {
  return new Promise((resolve: () => void, reject: (error: unknown) => void) => {
    FileSystem.unlink(item.path, (error: unknown) => {
      if (error) {
        reject(error);
      }
      else {
        resolve();
      }
    });
  });
}

/**
 * Removes a folder item asynchronously.
 * Used by removeDirefctory().
 * @async
 * @param item The item to remove; Assumes that the given item has already been checked as an existing folder.
 */
function _removeFolderAsyncVerified(path: string) {
  return new Promise((resolve: () => void, reject: (error: unknown) => void) => {
    FileSystem.rmdir(path, (error: unknown) => {
      if (error) {
        reject(error);
      }
      else {
        resolve();
      }
    });
  });
}

//#endregion

export { IDirItem } from './dir-item.interface';
