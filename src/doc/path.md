# Muffin Dev for Node - Node Helpers - API Documentation - Path utilities

## `asAbsolute`

```ts
function asAbsolute(path: string, absOrigin: string = null): string
```

Converts the given path into an absolute path.

- `path: string`: The absolute path you want to resolve the given path from. If null given, resolves the path from process.cwd()
- `absOrigin: string = null`: The absolute path you want to resolve the given path from. If null given, resolves the path from process.cwd()

Returns the resolved path, or the input path if it was already absolute.