# Muffin Dev for Node - Node Helpers

This module contains a set of utilities for Node JS framework.

## Installation

Install it locally with NPM by using the following command:

```bash
npm i @muffin-dev/node-helpers
```

## Usage

Import example with vanilla JS:

```js
const NodeHelpers = require('@muffin-dev/node-helpers');
// OR, if you need only a specific method
// const readdirAsync = require('@muffin-dev/node-helpers').readdirAsync;

NodeHelpers.readdirAsync('./')
.then(files => { console.log('Files in the current directory', files); })
.catch(error => { console.error(error); });
```

Import example with Typescript:

```ts
import * as NodeHelpers from '@muffin-dev/node-helpers';
// OR, if you need only a specific method
// import { readdirAsync } from '@muffin-dev/node-helpers';

NodeHelpers.readdirAsync('./')
.then(files => { console.log('Files in the current directory', files); })
.catch(error => { console.error(error); });
```

## Documentation

[=> See API Documentation](./doc/README.md)